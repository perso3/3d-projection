from shape import Shape


def read(file):
    lines = []
    with open(file, "r") as f:
        for line in f.readlines():
            lines.append(line.replace("\n", ""))

    lines = remove_blank_lines(lines)
    lines = remove_comments(lines)

    shape_count, indexes = count_shapes(lines)

    shape_lines = []

    if shape_count > 1:
        shape_lines.extend(separate_shapes(lines, indexes))
    else:
        shape_lines.append(lines)

    shapes = []
    for shape in shape_lines:
        name = shape[0].split(" ")[-1]
        point_lines = get_lines_by_tag(shape, "v")
        print(point_lines)
        texture_lines = get_lines_by_tag(shape, "vt")
        normal_lines = get_lines_by_tag(shape, "vn")
        face_lines = get_lines_by_tag(shape, "f")

        points = extract_points(point_lines)
        faces = extract_faces(face_lines)

        shapes.append(Shape(name, points, faces))

    return shapes


def remove_blank_lines(lines):
    blank_count = lines.count("")
    for i in range(blank_count):
        lines.remove("")

    return lines


def remove_all_spaces(a_list):
    new_list = []
    for i in a_list:
        if " " not in i:
            new_list.append(i)

    return new_list


def remove_comments(lines):
    new_lines = []
    for line in lines:
        if line[0] != "#":
            new_lines.append(line)

    return new_lines


def count_shapes(lines):
    indexes = []

    for i, line in enumerate(lines):
        if line[0] == "o":
            indexes.append(i)

    return len(indexes), indexes


def separate_shapes(lines, indexes):
    shape_lines = []

    for i, index in enumerate(indexes):

        if i == len(indexes) - 1:
            shape = lines[index:]
        else:
            shape = lines[index:indexes[i + 1]]

        shape_lines.append(shape)

    return shape_lines


def get_lines_by_tag(lines, tag):
    result = []

    for line in lines:
        if line.split(" ")[0] == tag:
            result.append(line)

    return result


def extract_points(lines):
    points = []
    print(lines)
    for line in lines:
        new_line = line.split(" ")
        new_line = remove_all_spaces(new_line)
        new_line = remove_blank_lines(new_line)
        x = float(new_line[1])
        y = float(new_line[2])
        z = float(new_line[3])
        points.append([x, y, z])
    print(points)
    return points


def extract_faces(lines):
    faces = []
    for line in lines:
        new_line = line.split(" ")
        new_line = remove_all_spaces(new_line)
        new_line = remove_blank_lines(new_line)
        face = []
        for i in range(1, len(new_line)):
            v, vt, vn = new_line[i].split("/")
            face.append(int(v) - 1)

        faces.append(face)

    return faces


if __name__ == '__main__':
    read("cube.obj")
