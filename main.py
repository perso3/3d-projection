import pygame
from pygame.locals import *
import time

from renderer import render_box, render_shape
from box import Box
from projection_functions import *
from obj_file_functions import read


def simulate():
    WINDOW_SIZE = (600, 600)

    pygame.init()

    framerate = 60
    time_per_frame = 1 / framerate
    last_time = time.time()
    frameCount = 0

    mouse_pos = None
    pause = False

    window = pygame.display.set_mode(WINDOW_SIZE)
    pygame.display.set_caption("")
    mainClock = pygame.time.Clock()

    simulation = True

    shapes = read("cube.obj")
    # box = Box([300, 300])

    angle = 0
    zoom = 100

    while simulation:
        delta_time = time.time() - last_time
        while delta_time < time_per_frame:
            delta_time = time.time() - last_time

            for event in pygame.event.get():

                if event.type == QUIT:
                    simulation = False

                if event.type == MOUSEMOTION:
                    mouse_pos = event.pos
                    # print(mouse_pos)

                if event.type == KEYDOWN:
                    if event.key == 32:
                        pause = not pause
                    elif event.key == 1073741886:
                        pass

                if event.type == MOUSEBUTTONDOWN:
                    pass

        if not pause:
            pass

        if frameCount % 1 == 0:
            # new_points = box.points
            #
            # new_points = rotateX(new_points, angle)
            # new_points = rotateY(new_points, angle)
            # new_points = rotateZ(new_points, angle)
            # projected_points = project(new_points)
            # render_box(window, box, projected_points)

            for shape in shapes:
                new_points = shape.points
                new_points = rotateX(new_points, angle)
                new_points = rotateY(new_points, angle)
                new_points = rotateZ(new_points, angle)
                projected_points = project(new_points)
                render_shape(window, shape, projected_points, zoom)

            angle += 0.01

        pygame.display.update()

        last_time = time.time()
        frameCount += 1


if __name__ == '__main__':
    simulate()