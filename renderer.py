import pygame


def clear(window):
    window.fill((0, 0, 0))


def render_connections(window, box, points):
    connections = box.connections

    color = (255, 255, 255)

    for connection in connections:
        x1 = box.origin[0] + points[connection[0]][0]
        y1 = box.origin[1] + points[connection[0]][1]
        x2 = box.origin[0] + points[connection[1]][0]
        y2 = box.origin[1] + points[connection[1]][1]

        start = [x1, y1]
        end = [x2, y2]

        pygame.draw.line(window, color, start, end)


def render_box(window, box, points):
    clear(window)

    color = (255, 0, 0)

    points = points
    render_connections(window, box, points)

    for point in points:
        x = box.origin[0] + point[0]
        y = box.origin[1] + point[1]
        pos = (x, y)
        pygame.draw.circle(window, color, pos, 5)


def render_faces(window, shape, points, face_color, zoom):
    faces = shape.faces

    for face in faces:
        face_points = []

        for index in face:
            a_point = points[index]
            x = shape.origin[0] + a_point[0] * zoom
            y = shape.origin[1] + a_point[1] * zoom

            face_points.append([x, y])

        pygame.draw.polygon(window, face_color, face_points)


def render_lines(window, shape, points, zoom):
    color = [0, 0, 255]
    for point_a in points:
        x1 = shape.origin[0] + point_a[0] * zoom
        y1 = shape.origin[1] + point_a[1] * zoom
        start = (x1, y1)
        for point_b in points:
            x2 = shape.origin[0] + point_b[0] * zoom
            y2 = shape.origin[1] + point_b[1] * zoom
            end = (x2, y2)

            pygame.draw.line(window, color, start, end)


def render_shape(window, shape, points, zoom):
    clear(window)

    point_color = (255, 0, 0)
    face_color = (255, 255, 255)

    points = points
    render_faces(window, shape, points, face_color, zoom)
    render_lines(window, shape, points, zoom)
    for point in points:
        x = shape.origin[0] + point[0] * zoom
        y = shape.origin[1] + point[1] * zoom
        pos = (x, y)

        pygame.draw.circle(window, point_color, pos, 1)
