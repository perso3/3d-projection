import numpy as np
import math


def rotateZ(points, angle):

    rotation = np.matrix([[math.cos(angle), -math.sin(angle), 0], [math.sin(angle), math.cos(angle), 0], [0, 0, 1]])

    new_points = []
    for point in points:
        new_point = np.matmul(rotation, point).base
        new_points.append(new_point)

    return new_points


def rotateX(points, angle):

    rotation = np.matrix([[1, 0, 0], [0, math.cos(angle), -math.sin(angle)], [0, math.sin(angle), math.cos(angle)]])

    new_points = []
    for point in points:
        new_point = np.matmul(rotation, point).base
        new_points.append(new_point)

    return new_points


def rotateY(points, angle):

    rotation = np.matrix([[math.cos(angle), 0, -math.sin(angle)], [0, 1, 0], [math.sin(angle), 0, math.cos(angle)]])

    new_points = []
    for point in points:
        new_point = np.matmul(rotation, point).base
        new_points.append(new_point)

    return new_points


def project(points):
    projection = np.matrix([[1, 0, 0], [0, 1, 0]])

    new_points = []
    for point in points:
        new_point = np.matmul(projection, point).base
        new_points.append(new_point)

    return new_points
