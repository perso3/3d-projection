import numpy as np


class Shape:

    def __init__(self, name, points, faces, origin=[300, 300]):
        self.name = name
        self.origin = origin
        self.points = np.array(points)
        self.faces = faces
